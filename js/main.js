(function ($) {
    $(function () {
        $('.slider').slick({
            dots: true,
            arrows: true,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
           /* autoplay: true,
            autoplaySpeed: 3500*/
//        adaptiveHeight: true
        });
    });
})(jQuery);


(function($){
    $(function() {
        $('.menu__icon').on('click', function() {
            $(this).closest('.menu').toggleClass('menu_state_open');
        });
    });
})(jQuery);

